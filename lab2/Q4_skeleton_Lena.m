close all
clear all

%% Displaying Lena image data

nom_fich_in= 'lena.tif' ;
lennaim= imread(nom_fich_in) ;

% Data point: vector value of color pixel (p=3) 
p=3 ;

% Get the sample size N (here data are trivariate, i.e. p=3)
n= numel(lennaim) / p ;

figure,
subplot(3,3,1)
imshow(lennaim) ;

%% Question 4.2 Vector Quantification 

options = statset('MaxIter',1000) ;
lennaim= double( reshape(lennaim, n , p) ) ; %Vectorize dataset 

K= 2 ; % binary clustering

% Compute kmeans cluster indexes IDX, cluster centroids C, and 
% within-cluster sums of point-to-centroid distances sumd
[IDX,C,sumd] = kmeans(lennaim,K,'replicates',5, 'options',options) ;

% Compute quantized image 
for k=1:K
    lennaquantK( IDX == k , : )= repmat( C(k,:), sum(IDX == k) , 1)  ; 
end
% Display quantized image
imshow(uint8(reshape( lennaquantK, 512,512,p ))) ;

%% TODO: questions 4.3 and 4.4