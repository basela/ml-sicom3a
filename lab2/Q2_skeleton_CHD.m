close all
clear all
%% Question 2.1  Loading/Displaying CHD data
nom_fich_in= 'chd_train.mat' ;
load(nom_fich_in) ; % import dataset into variables Xtrain (and Ytrain for case/control labels) 

K=2 ; % binary clustering

% Get the sample size N (here data are univariate, i.e. p=1)
N= length(Xtrain) ;

% display an histogram of the data
figure,
hold on
histogram(Xtrain,'BinMethod','integers','BinWidth',5) 

%% Question 2.2. 
options=statset( 'MaxIter', 500, 'TolX', 1e-8, 'TolFun',1e-10 ) ;

% Retain the best partition (GMM likelihood sense) from 10 random
% iniatilizations
mdlGMM = fitgmdist(Xtrain,K,'Replicates',10,'Options',options) ;
% Get the partition 
idx = cluster(mdlGMM,Xtrain) ;
% Get the cluster parameters
mu0= mdlGMM.mu(1) ;
mu1= mdlGMM.mu(2) ;
sig0=squeeze( mdlGMM.Sigma(1,1,1) ) ;
sig1=squeeze( mdlGMM.Sigma(1,1,2) ) ;
pi0=squeeze( mdlGMM.PComponents(1) ) ;
pi1=squeeze( mdlGMM.PComponents(2) ) ;

xval= linspace(10,70,1000) ;

% plot the GMM pdf
figure,
hold on
ezplot(@(x)pdf(mdlGMM,x), [10 70 0 .1] )  % Recent versions of matlab warn to replace ezplot() by fimplicit(), but that's okay

% TODO: 
% compute and display on the same figure the pdf g0 and g1 for each cluster

%% Question 2.4 
% TODO: Use responses Ytrain to compare
%  - case/controls classes with clusters
%  -EM/GMM clustering vs QDA supervised classification

