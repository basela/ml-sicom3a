close all
clear all

%% Question 1.1 and 1.4. Displaying Fictitious data

nom_fich_in= 'fictitious_train.mat' ;
load( nom_fich_in ) ; % import dataset into variables Xtrain

% Get the sample size N (here data are univariate, i.e. p=1)
N=length(Xtrain) ; 

% Display datapoints and their histogram
figure,
hold on
bins= min(Xtrain)-1:1:max(Xtrain)+1 ;
hist(Xtrain,bins) ;
plot(Xtrain,zeros(size(Xtrain)),'xr','MarkerSize',14) ;


%% Question 1.2. Implement Kmeans

K=2 ; % number of clusters

% Initialization
change= true ; % flag for the stopping criterion  
perm = randperm( N , K ) ; % random initialization to pick up centroids
idx= zeros(N,1) ;   % cluster indexes/labels
muvec= zeros(K,1) ; % mean values for each cluster 

% Choose data points with random indexes perm(k) as starting centroid values 
for k=1:K
    muvec(k)= Xtrain( perm(k) ,: ) ;
end

% Compute the initial labels for each data point
for i=1:N
    % Find the index indm of the centroid whose cluster is the closest 
    % from  Xtrain(i)
    [vm, indm] = min( mean( (Xtrain(i) - muvec ).^2 , 2 ) ) ;
    idx(i)= indm ;  
end

% loop until convergence
while (change)
    
    change= false  ;
    % Update
    for k=1:K
        muvec(k)= 0 ; % FIXME
    end
    % Assigment
    for i=1:N
        
        indm=1 ; %FIXME: compute the index of the cluster where Xtrain(i) is assigned
        
        if ( indm ~=  idx(i) )
            idx(i)= indm ;
            change= true ;
        end
    end
end

%% Display clusters and decision boundary

figure,
hold on
plot(Xtrain,zeros(size(Xtrain)),'xk','MarkerSize',14) ;
plot(muvec(1),0,'pg','MarkerSize',16) ;
plot(muvec(2),0,'pb','MarkerSize',16) ;
plot([ mean(muvec) mean(muvec)] , [0 1], '-r' ) ;


%% Question 1.3. Use Matlab kmeans built-in functions

% same initialization as previous question
[idxkm centroidkm]= kmeans( Xtrain, K, 'start', Xtrain(perm) ) ;

%TODO: compare the results with your kmeans implementation, i.e.
% - muvec VS centroidkm
% - indx  VS idxkm


%% Question 1.5. Implement EM for Gaussian Mixture Model (GMM) clustering

K=2 ;
MaxIter= 100 ; % stopping criterion


% Initialization
pivec= ones(K,1)* 1/K ; % initial prior weights for each cluster
perm = randperm( N , K ) ; % random initialization to pick up centroids
muvec= zeros(K,1) ; % mean values for each cluster 
sigvec= ones(K,1) ; % variance values sigma^2 for each cluster 
postpr= zeros(N,K) ; % posterior proba/weight for each cluster and each data point

% Choose data points with random indexes perm(k) as starting centroid values 
for k=1:K
    muvec(k,:)= Xtrain( perm(k) ,: ) ;
    sigvec(k,:)= var(Xtrain) ;
end

% Loop until maximum number of iterations is reached
for t=1:MaxIter
    
    % E step: Prediction step
    for i=1:N
        % Compute the normal pdfs p( X_i | Y_i=k ) for current parameters,
        % and for each cluster k=1,2
        g1=  normpdf(Xtrain(i), muvec(1,:),  sqrt( sigvec(1,:) ) ) ;
        g2=  normpdf(Xtrain(i), muvec(2,:),  sqrt( sigvec(2,:) ) ) ;
        
        % Compute marginal distribution of data point X_i
        % p(X_i)= \sum_k \pi_k * normpdf(X_i, \mu_k, \sigma_k )
        px= pivec(1) * g1(i) + pivec(2) * g2(i)        
        
        % Compute proba p( Y_i | X_i=k ) for each cluster k=1,2
        postpr(i,1)= 0.5 ; % (cluster k=1) FIXME
        postpr(i,2)= 0.5 ; % (cluster k=2) FIXME
    end
        
    % M step: Update step
    for k=1:K
        pivec(k,:)=  mean( postpr(:,k) ) ; % new prior cluster weight
        muvec(k,:)= sum( postpr(:,k) .* Xtrain ) ./  sum( postpr(:,k) ) ; % new cluster mean
        sigvec(k,:)= 1 ; %FIXME: compute new cluster variance, for each cluster
    end
    
end


%% Question 1.6: Display pdf estimates
figure,
title( 'density estimates' ) ;
hold on
plot(Xtrain,zeros(size(Xtrain)),'xk','MarkerSize',14) ;
xval= linspace(-1,7,1000) ;
g1= normpdf(xval, muvec(1), sqrt( sigvec(1) ) ) ;
g2= normpdf(xval, muvec(2), sqrt( sigvec(2) ) ) ;
g= pivec(1) * g1 + pivec(2) * g2 ;
plot( xval, g1, '-c' ) ;
plot( xval, g2, '-m' ) ;
plot( xval, g, '-b' ) ;

%% Question 1.7:  Use Matlab EM/GMM built-in functions

p= 1 ; % Here data points are univariate 
strParIni.PComponents= pivec ;
strParIni.mu= muvec ;
strParIni.Sigma= zeros(p,p,K) ;
strParIni.Sigma(1,1,:)= sigvec ;


% Options for better precision
% options=statset( 'MaxIter', 200, 'TolX', 1e-8, 'TolFun',1e-10 ) ;
%
% obj= gmdistribution.fit(Xtrain,K, 'replicates', 1, 'options', options, ...
%'start', strParIni) ;

obj= gmdistribution.fit(Xtrain,K, 'replicates', 1, 'start', strParIni) ;

% Get cluster centroids and weights
gmestPi= obj.PComponents ;
gmestMu= obj.mu' ;
gmestSig= squeeze( obj.Sigma )' ;

%TODO: compare the results with your kmeans implementation, i.e.
% - prior weights: pivec VS gmestPi
% - cluster means: muvec  VS gmestMu
% - cluster variance sigvec VS gmestSig


%% Question 1.8: Kmeans vs EM
% TODO: plot both 
%  - kmeans decision boudary (hard thresholding) 
%  - GMM responsabilities curves (soft thresholding)