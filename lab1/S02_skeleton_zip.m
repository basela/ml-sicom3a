close all
clear all
%% Zip Codes

% Load zip data sets
nom_fich_train= 'zip_train.mat' ;
nom_fich_test= 'zip_test.mat' ;
load(nom_fich_train) ; % import training dataset into variables Xtrain, Ytrain
load(nom_fich_test) ;  % import test dataset into variables Xtest, Ytest

% Get the sample size n and the data point dimension p (here p=256)
[n,p]= size( Xtrain )
% Multiclass problem: one class per digit
K=10 ; 

% Display some examples (Lab question 2.1)
figure,
title('Examples of sample');
subplot(4,3,1) ;
for k=0:K-1
    indk= find(Ytrain==k,1,'first') ;
    subplot(4,3,k+1) ;
    imshow( ( reshape(Xtrain(indk,:),16,16) + 1 )'*.5 )  ;
end


%% Lab Question 2.4 
% Display some data generated with the LDA model that fit the training set

% Get the common pooled covariance matrix  (LDA assumption)
SigmaLDA= zeros(p) ;
for k=0:K-1
    indk= (Ytrain==k) ;
    nk= sum(indk) ;
    SigmaLDA= SigmaLDA + ( cov( Xtrain(indk,:) ) ) * ( nk -1) ;
end
SigmaLDA= diag( diag(SigmaLDA) ) / ( numel(Ytrain) - K ) ; 


if (0) %FIXME
    figure,
    title('Realizations from the LDA generative model');
    subplot(4,3,1) ;
    for k=0:K-1

        % Get the sample mean for each class separately 
        muk= zeros(1,p) ; %FIXME

        subplot(4,3,k+1) ;
        % generate a gaussian random vector with LDA parameters
        genk= muk + randn(1,p) * sqrt(SigmaLDA)  ;
        % display as an image
        imshow((reshape(genk,16,16)+1)'*.5 )  ;
    end
end


%% Lab Question 3.2, 3.3, 3.4 Binary SVM classification

% Binary SVM classif between the pair of most confused classes
label1= 1 ; % FIXME
label2= 2 ; % FIXME

% Retain only the data associated with these two classes to perform
% binary SVM classification

indl1= (Ytrain== label1) ;
indl2= (Ytrain== label2) ;
Ytrainl12= [Ytrain(indl1); Ytrain(indl2)] ;
Xtrainl12= [Xtrain(indl1,:); Xtrain(indl2,:)] ;

indtestl1= (Ytest== label1) ;
indtestl2= (Ytest== label2) ;
Ytestl12= [Ytest(indtestl1); Ytest(indtestl2)] ;
Xtestl12= [Xtest(indtestl1,:); Xtest(indtestl2,:)] ;

% Perform K-fold Cross Validation for optimizing SVM hyperparams

% Create K-fold partition
nl12= numel(Ytrainl12) ; 
c = cvpartition(nl12,'KFold',10); % 10 fold

% Evaluate test error for default values of the SVM parameters

z.box= 1 ; % Box constraint hyperparameter to penalize slack variables
z.sigma= 10 ; % Kernel bandwidth hyperparameter (RBF kernel)
mcr12= kfoldLoss( fitcsvm(Xtrainl12,Ytrainl12,'CVPartition',c,...
        'KernelFunction','rbf','BoxConstraint',z.box,...
        'KernelScale',z.sigma))
% TODO: test different orders of magnitude for these two parameters
    